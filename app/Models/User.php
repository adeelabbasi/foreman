<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles; 

class User extends Authenticatable
{
    use Notifiable;
	use HasRoles;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type', 'username', 'avatar', 'job_id', 'superintendent_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];	
	
	public function Crew()
	{
		return $this->hasMany('App\Models\Crew' , 'superintendent_id');
	}
	
	public function Crew_report()
	{
		return $this->hasMany('App\Models\Crew_report' , 'foreman_id');
	}
	
	public function Question()
	{
		return $this->hasMany('App\Models\Question' , 'question_id');
	}
	
	public function Answer()
	{
		return $this->hasMany('App\Models\Answer' , 'answer_id');
	}
	
	public function Job()
	{
		return $this->belongsTo('App\Models\Job' , 'job_id');
	}
	
	public function Superintendent()
	{
		return $this->belongsTo('App\Models\User' , 'superintendent_id');
	}
}
