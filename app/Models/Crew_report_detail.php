<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Crew_report_detail extends Model
{
	protected $table = 'crew_report_details';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'crew_id', 'report_id', 'hours'
    ];
	
	public function Superintendent_report()
	{
		return $this->belongsTo('App\Models\User' , 'report_id');
	}
	
	public function Foreman_report()
	{
		return $this->belongsTo('App\Models\User' , 'report_id');
	}
	
	public function Crew()
	{
		return $this->belongsTo('App\Models\Crew' , 'crew_id');
	}
}
