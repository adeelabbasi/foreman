<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'title'
    ];

	public function User()
	{
		return $this->hasMany('App\Models\User' , 'id');
	}
	
	public function Crew()
	{
		return $this->hasMany('App\Models\Crew' , 'id');
	}
}
