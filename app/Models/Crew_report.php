<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Crew_report extends Model
{
	protected $table = 'crew_reports';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'foreman_id', 'superintendent_id', 'status', 'comments'
    ];
	
	public function Foreman()
	{
		return $this->belongsTo('App\Models\User' , 'foreman_id');
	}
	
	public function Superintendent()
	{
		return $this->belongsTo('App\Models\User' , 'superintendent_id');
	}
	
	public function Crew_report_detail()
	{
		return $this->hasMany('App\Models\Crew_report_detail' , 'report_id');
	}
	
	public function Question()
	{
		return $this->hasMany('App\Models\Question' , 'report_id');
	}

}
