<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'user_id', 'question', 'report_id'
    ];

	public function User()
	{
		return $this->belongsTo('App\Models\User' , 'user_id');
	}
	
	public function Report()
	{
		return $this->belongsTo('App\Models\Crew_report' , 'report_id');
	}


	public function Answer()
	{
		return $this->hasMany('App\Models\Answer' , 'question_id');
	}
}
