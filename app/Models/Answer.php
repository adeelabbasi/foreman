<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'user_id', 'answer', 'question_id'
    ];

	public function User()
	{
		return $this->belongsTo('App\Models\User' , 'user_id');
	}
	
	public function Question()
	{
		return $this->belongsTo('App\Models\Question' , 'user_id');
	}
}
