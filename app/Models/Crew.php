<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Crew extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'user_id', 'name', 'job_id'
    ];
	
	public function Crew_report_detail()
	{
		return $this->hasMany('App\Models\Crew_report_detail' , 'id');
	}
	
	public function User()
	{
		return $this->belongsTo('App\Models\User' , 'user_id');
	}
	
	public function Job()
	{
		return $this->belongsTo('App\Models\Job' , 'job_id');
	}
}
