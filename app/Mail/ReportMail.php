<?php

namespace App\Mail;

use App\Models\Crew_report;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReportMail extends Mailable
{
    use Queueable, SerializesModels;
	
	/**
     * The order instance.
     *
     * @var Order
     */
    public $crew_report;
	
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Crew_report $crew_report)
    {
        $this->crew_report = $crew_report;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@report.com')
                ->markdown('emails.report');
    }
}
