<?php

namespace App\Repositories;

use App\Models\Question;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class QuestionRepository {
	
	/**
	 * @var App\Models\Question
	 */
	protected $db_question;
		
    public function __construct(Question $db_question) 
    {
        $this->db_question = $db_question;
    }
	
	public function addQuestion($inputs)
    {
        $db_question = $this->storeQuestion(new $this->db_question ,  $inputs);
        return $db_question;
    }
	
	public function updateQuestion($inputs, $id)
	{
		$db_question = $this->db_question->findOrFail($id);
		$question_id = $this->storeQuestion($db_question, $inputs, $id);
		return $question_id;
	}
	
	public function deleteQuestion($id)
    {
		$db_question = $this->db_question->findOrFail($id);
        $db_question->delete();
        return true;
    }

	function storeQuestion($db_question , $inputs, $id = null)
	{	
		$db_question->question = $inputs['question'];
		$db_question->user_id = $inputs['user_id'];
		$db_question->report_id = $inputs['report_id'];
		$db_question->save();
		return $db_question;
	}
	
	public function getQuestion($id = null)
    {
		if($id==null)
		{
			$info_Question = $this->db_question->select('id', 'user_id', 'question', 'report_id')->orderBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_Question = $this->db_question->select('id', 'user_id', 'question', 'report_id')->findOrFail($id);
		}
        return $info_Question;
    }
	
	public function getForemanQuestion($ForemanID)
    {
		$info_Question = $this->db_question->select('id', 'user_id', 'question', 'report_id')->where('user_id', '=', $ForemanID)->orderBy('created_at', 'DESC')->get();
        return $info_Question;
    }
	
	public function getReportQuestion($ReportID)
    {
		$info_Question = $this->db_question->select('id', 'user_id', 'question', 'report_id')->where('report_id', '=', $ReportID)->orderBy('created_at', 'DESC')->get();
        return $info_Question;
    }
	
	public function getForemanReportQuestion($ReportID, $ForemanID)
    {
		$info_Question = $this->db_question->select('id', 'user_id', 'question', 'report_id')->where('report_id', '=', $ReportID)->where('user_id', '=', $ForemanID)->orderBy('created_at', 'DESC')->get();
        return $info_Question;
    }
}

