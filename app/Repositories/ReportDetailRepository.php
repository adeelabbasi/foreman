<?php

namespace App\Repositories;

use App\Models\Crew_report;
use App\Models\Crew_report_detail;
use Illuminate\Support\Facades\Input;
use DB;
use Auth; 

class ReportDetailRepository {
	
	/**
	 * @var App\Models\User
	 */
	protected $db_crew_report;
	protected $db_crew_report_detail;
	
    public function __construct(Crew_report $db_crew_report, Crew_report_detail $db_crew_report_detail) 
    {
        $this->db_crew_report = $db_crew_report;
		$this->db_crew_report_detail = $db_crew_report_detail;
    }
	
	public function addCrewReportDetail($inputs)
    {
        $db_crew_report_detail = $this->storeCrewReportDetail(new $this->db_crew_report_detail ,  $inputs);
        return $db_crew_report_detail;
    }
	
	public function updateCrewReportDetail($inputs, $id)
	{
		$db_crew_report_detail = $this->db_crew_report_detail->findOrFail($id);
		$crew_id = $this->storeCrewReportDetail($db_crew_report_detail, $inputs, $id);
		return $crew_id;
	}
	
	public function deleteCrewReportDetail($id)
    {
		$db_crew_report_detail = $this->db_crew_report_detail->findOrFail($id);
        $db_crew_report_detail->delete();
        return true;
    }

	function storeCrewReportDetail($db_crew_report_detail , $inputs, $id = null)
	{	
		$db_crew_report_detail->crew_id = $inputs['crew_id'];
		$db_crew_report_detail->report_id = $inputs['report_id'];
		$db_crew_report_detail->hours = $inputs['hours'];
		$db_crew_report_detail->save();
		return $db_crew_report_detail;
	}
	
	public function getCrewReportDetail($id = null)
    {
		if($id==null)
		{
			$info_CrewReportDetail = $this->db_crew_report_detail->select('id', 'crew_id', 'report_id', 'hours')->orderBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_CrewReportDetail = $this->db_crew_report_detail->select('id', 'crew_id', 'report_id', 'hours')->findOrFail($id);
		}
        return $info_CrewReportDetail;
    }
}

