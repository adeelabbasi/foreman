<?php

namespace App\Repositories;

use App\Models\Crew_report;
use App\Models\Crew_report_detail;
use Illuminate\Support\Facades\Input;
use DB;
use Auth; 

class ReportRepository {
	
	/**
	 * @var App\Models\User
	 */
	protected $db_crew_report;
	protected $db_crew_report_detail;
	
    public function __construct(Crew_report $db_crew_report, Crew_report_detail $db_crew_report_detail) 
    {
        $this->db_crew_report = $db_crew_report;
		$this->db_crew_report_detail = $db_crew_report_detail;
    }
	
	public function addCrewReport($inputs)
    {
        $db_crew_report = $this->storeCrewReport(new $this->db_crew_report ,  $inputs);
        return $db_crew_report;
    }
	
	public function updateCrewReport($inputs, $id)
	{
		$db_crew_report = $this->db_crew_report->findOrFail($id);
		$crew_id = $this->storeCrewReport($db_crew_report, $inputs, $id);
		return $crew_id;
	}
	
	public function deleteCrewReport($id)
    {
		$db_crew_report = $this->db_crew_report->findOrFail($id);
        $db_crew_report->delete();
        return true;
    }

	function storeCrewReport($db_crew_report , $inputs, $id = null)
	{	
		if(isset($inputs['foreman_id']))
			$db_crew_report->foreman_id = $inputs['foreman_id'];
		if(isset($inputs['superintendent_id']))
			$db_crew_report->superintendent_id = $inputs['superintendent_id'];
		$db_crew_report->status = $inputs['status'];
		if(isset($inputs['comments']))
			$db_crew_report->comments = $inputs['comments'];
		$db_crew_report->save();
		return $db_crew_report;
	}
	
	public function getCrewReport($id = null)
    {
		if($id==null)
		{
			$info_CrewReport = $this->db_crew_report->select('id', 'foreman_id', 'superintendent_id', 'status', 'created_at', 'updated_at', 'comments')->orderBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_CrewReport = $this->db_crew_report->select('id', 'foreman_id', 'superintendent_id', 'status', 'created_at', 'updated_at', 'comments')->findOrFail($id);
		}
        return $info_CrewReport;
    }
	
	public function getUserCrewReport($ForemanID)
    {
		$info_CrewReport = $this->db_crew_report->select('id', 'foreman_id', 'superintendent_id', 'status', 'created_at', 'updated_at', 'comments')->where('foreman_id', '=',$ForemanID)->orderBy('created_at', 'DESC')->get();
        return $info_CrewReport;
    }
	
	public function ApprovedReport($ReportID)
	{
		$db_crew_report = $this->db_crew_report->findOrFail($ReportID);
		$db_crew_report->superintendent_id = Auth::user()->id;
		$db_crew_report->status = 1;
		$db_crew_report->save();
		return $db_crew_report;
	}
}

