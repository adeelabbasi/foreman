<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Input;
use App\Repositories\ImageRepository;
use DB;
use Illuminate\Support\Facades\Hash;
use Auth; 

class UserRepository {
	
	/**
	 * @var App\Models\User
	 */
	protected $db_user;
	protected $imageRps;
	
	protected $uploadFolder = 'media/avatar/';
	
    public function __construct(User $db_user, ImageRepository $imageRps) 
    {
        $this->db_user = $db_user;
		$this->imageRps = $imageRps;
    }
	
	public function addUser($inputs)
    {
        $db_user = $this->storeUser(new $this->db_user ,  $inputs);
        return $db_user;
    }
	
	public function updateUser($inputs, $id)
	{
		$db_user = $this->db_user->findOrFail($id);
		$db_user = $this->storeUser($db_user, $inputs, $id);
		return $db_user;
	}
	
	public function updateUserPassword($inputs, $id)
	{
		$db_user = $this->db_user->findOrFail($id);
		$db_user->password = Hash::make($inputs['newpassword']);
		$db_user->username = $inputs['username'];
		$user_id = $db_user->save();
		return $user_id;
	}
	
	public function deleteUser($id)
    {
		$db_user = $this->db_user->findOrFail($id);
		$this->imageRps->delete($db_user->avatar , $this->uploadFolder);
        $db_user->delete();
        return true;
    }

	function storeUser($db_user , $inputs, $id = null)
	{
		if ($id) 
		{
			$db_user->id = $id;
			if(isset($inputs['avatar']))
			{
				$this->imageRps->delete($db_user->avatar, $this->uploadFolder);
				$db_user->avatar = $this->imageRps->upload($inputs['avatar'], $id, $this->uploadFolder, '300', '300');
			}
		}
		else
		{
			$db_user->email = $inputs['email'];
			$db_user->password = Hash::make($inputs['password']);
			$db_user->avatar = "";
		}
		
		$db_user->name = $inputs['name'];
		$db_user->username = $inputs['username'];
		$db_user->email = $inputs['email'];
		
		if(isset($inputs['password']))
			$db_user->password = Hash::make($inputs['password']);
		
		if(isset($inputs['job_id']))
			$db_user->job_id = $inputs['job_id'];
		
		if(isset($inputs['superintendent_id']))
			$db_user->superintendent_id = $inputs['superintendent_id'];
			
		$db_user->type = $inputs['type'];
		$db_user->save();
		
		if(Auth::User()->getRoleNames()[0] == "SuperAdmin" && $id == null)
		{
			if($inputs['type']==2)
			{
				$db_user->assignRole('Superintendent');
			}
			else
			if($inputs['type']==3)
			{
				$db_user->assignRole('Foreman');
			}
		}
		
		return $db_user;
	}
	
	public function getUser($type = null, $id = null)
    {
		if($id==null)
		{
			if($id==null)
			{
				$info_User = $this->db_user->select('id', 'name', 'email', 'password', 'type', 'username', 'avatar', 'job_id','superintendent_id')->where('type','=', $type)->orderBy('created_at', 'DESC')->get();
			}
			else
			{
				$info_User = $this->db_user->select('id', 'name', 'email', 'password', 'type', 'username', 'avatar', 'job_id','superintendent_id')->orderBy('created_at', 'DESC')->get();
			}
		}
		else
		{
			$info_User = $this->db_user->select('id', 'name', 'email', 'password', 'type', 'username', 'avatar', 'job_id','superintendent_id')->findOrFail($id);
		}
        return $info_User;
    }
}

