<?php

namespace App\Repositories;

use App\Models\Crew;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class CrewRepository {
	
	/**
	 * @var App\Models\Crew
	 */
	protected $db_crew;
		
    public function __construct(Crew $db_crew) 
    {
        $this->db_crew = $db_crew;
    }
	
	public function addCrew($inputs)
    {
        $db_crew = $this->storeCrew(new $this->db_crew ,  $inputs);
        return $db_crew;
    }
	
	public function updateCrew($inputs, $id)
	{
		$db_crew = $this->db_crew->findOrFail($id);
		$crew_id = $this->storeCrew($db_crew, $inputs, $id);
		return $crew_id;
	}
	
	public function deleteCrew($id)
    {
		$db_crew = $this->db_crew->findOrFail($id);
        $db_crew->delete();
        return true;
    }

	function storeCrew($db_crew , $inputs, $id = null)
	{	
		if($inputs['user_id']!=0)
		{
			$db_crew->user_id = $inputs['user_id'];
		}
		$db_crew->job_id = $inputs['job_id'];
		$db_crew->name = $inputs['name'];
		$db_crew->save();
		return $db_crew;
	}
	
	public function getCrew($id = null)
    {
		if($id==null)
		{
			$info_Crew = $this->db_crew->select('id', 'user_id', 'job_id', 'name')->orderBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_Crew = $this->db_crew->select('id', 'user_id', 'job_id', 'name')->findOrFail($id);
		}
        return $info_Crew;
    }
	
	public function getForemanCrew($foremanID)
    {

		$info_Crew = $this->db_crew->select('id', 'user_id', 'job_id', 'name')->where('user_id','=',$foremanID)->orderBy('created_at', 'DESC')->get();
        return $info_Crew;
    }
}

