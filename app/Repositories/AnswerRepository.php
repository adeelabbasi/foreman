<?php

namespace App\Repositories;

use App\Models\Answer;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class AnswerRepository {
	
	/**
	 * @var App\Models\Answer
	 */
	protected $db_answer;
		
    public function __construct(Answer $db_answer) 
    {
        $this->db_answer = $db_answer;
    }
	
	public function addAnswer($inputs)
    {
        $db_answer = $this->storeAnswer(new $this->db_answer ,  $inputs);
        return $db_answer;
    }
	
	public function updateAnswer($inputs, $id)
	{
		$db_answer = $this->db_answer->findOrFail($id);
		$answer_id = $this->storeAnswer($db_answer, $inputs, $id);
		return $answer_id;
	}
	
	public function deleteAnswer($id)
    {
		$db_answer = $this->db_answer->findOrFail($id);
        $db_answer->delete();
        return true;
    }

	function storeAnswer($db_answer , $inputs, $id = null)
	{	
		$db_answer->answer = $inputs['answer'];
		$db_answer->user_id = $inputs['user_id'];
		$db_answer->question_id = $inputs['question_id'];
		$db_answer->save();
		return $db_answer;
	}
	
	public function getAnswer($id = null)
    {
		if($id==null)
		{
			$info_Answer = $this->db_answer->select('id', 'user_id', 'answer', 'question_id')->orderBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_Answer = $this->db_answer->select('id', 'user_id', 'answer', 'question_id')->findOrFail($id);
		}
        return $info_Answer;
    }
}

