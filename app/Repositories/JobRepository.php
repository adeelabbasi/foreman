<?php

namespace App\Repositories;

use App\Models\Job;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class JobRepository {
	
	/**
	 * @var App\Models\Job
	 */
	protected $db_job;
		
    public function __construct(Job $db_job) 
    {
        $this->db_job = $db_job;
    }
	
	public function addJob($inputs)
    {
        $db_job = $this->storeJob(new $this->db_job ,  $inputs);
        return $db_job;
    }
	
	public function updateJob($inputs, $id)
	{
		$db_job = $this->db_job->findOrFail($id);
		$job_id = $this->storeJob($db_job, $inputs, $id);
		return $job_id;
	}
	
	public function deleteJob($id)
    {
		$db_job = $this->db_job->findOrFail($id);
        $db_job->delete();
        return true;
    }

	function storeJob($db_job , $inputs, $id = null)
	{	
		$db_job->title = $inputs['title'];
		$db_job->save();
		return $db_job;
	}
	
	public function getJob($id = null)
    {
		if($id==null)
		{
			$info_Job = $this->db_job->select('id', 'title')->orderBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_Job = $this->db_job->select('id', 'title')->findOrFail($id);
		}
        return $info_Job;
    }
}

