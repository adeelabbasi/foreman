<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class RedirectIfRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $Type)
    {
        if ( Auth::check() && Auth::user()->type== $Type )
        {
            return $next($request);
        }
        return redirect('404');
    }
}
