<?php
namespace App\Http\Controllers\Superintendent;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\QuestionRepository;
use App\Repositories\UserRepository;
use App\Repositories\ReportRepository;
use App\Models\Question;
use DataTables;
use URL;
use Auth;
use Session;

class QuestionController extends Controller
{
	protected $questionRps;
	protected $foremanRps;
	protected $reportRps;
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(QuestionRepository $questionRps, UserRepository $foremanRps, ReportRepository $reportRps)
    { 
		$this->middleware('auth');
		$this->reportRps = $reportRps;
        $this->questionRps = $questionRps;
		$this->foremanRps = $foremanRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Superintendent.Question.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$info_CrewReport = $this->reportRps->getCrewReport();
		$info_Foreman = $this->foremanRps->getUser(3);
        return view('Superintendent.Question.add', compact('info_Foreman','info_Foreman'),compact('info_CrewReport','info_CrewReport'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		foreach($request["question"] as $key => $question)
		{
			$input = array("question" => $question, "user_id" => $request["user_id"][$key], "report_id" => $request["report_id"][$key]);
			$this->questionRps->addQuestion($input);
		}
		Session::flash('flash_message', 'Question successfully added!');
		return view('Superintendent.Question.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $info_Question = $this->questionRps->getQuestion($id);
		return view('Superintendent.Question.show' ,array('info_Question' => $info_Question));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$info_CrewReport = $this->reportRps->getCrewReport();
		$info_Foreman = $this->foremanRps->getUser(3);
        $info_Question = $this->questionRps->getQuestion($id);
		return view('Superintendent.Question.edit' ,array('info_Question' => $info_Question, 'info_Foreman' => $info_Foreman),compact('info_CrewReport','info_CrewReport'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->questionRps->updateQuestion($request->all() , $id);
		Session::flash('flash_message', 'Question successfully updated!');
		return view('Superintendent.Question.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->questionRps->deleteQuestion($id);
    }
	
	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function UpdateAnswer(Request $request)
    {
		$inputs = $request->all();
		$this->answerRps->updateAnswer($inputs,$inputs["answer_id"]);    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   $info_Question = $this->questionRps->getQuestion();
	   return Datatables::of($info_Question)
	   	->editColumn('user_id', function ($info_Question) {
				 if($info_Question->user_id==0)
				 {
					 return "None";
				 }
				 else
				 {
					return $info_Question->User()->First()->name;
				 }
        })
		->editColumn('id', function ($info_Question) {
				return '<a href="/Superintendent/Question/'.$info_Question->id.'">QUESTION-'.$info_Question->id.'</a>';
        })
		->editColumn('report_id', function ($info_Question) {
				if($info_Question->report_id!="")
					return '<a href="/Superintendent/Report/'.$info_Question->Report()->First()->id.'">CREWREPORT-'.$info_Question->Report()->First()->id.'</a>';
				else
					return "";
        })
		->addColumn('answer', function ($info_Question) {
				 if($info_Question->Answer()->Get()->Count()==0)
				 {
					 return '';
					 
				 }
				 else
				 { 
				 	return $info_Question->Answer()->First()->answer;
				 }
        })
		->addColumn('edit', function ($info_Question) {
				 return '<div class="btn-group btn-group-action">
								<a class="btn btn-info" style="margin-right:2px;" href="'.url('/Superintendent/Question/'.$info_Question->id.'/edit').'" title="Edit Data"><i class="fa fa-pencil"></i></a> 
								<a class="btn btn-danger" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-remote="/Superintendent/Question/' . $info_Question->id . '"><i class="fa fa-trash"></i></a>
						</div>';
        })
		->escapeColumns([])
		->removeColumn('type')
		->make(true);
    }
}
