<?php

namespace App\Http\Controllers\Superintendent;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ReportRepository;
use App\Repositories\ReportDetailRepository;
use App\Repositories\CrewRepository;
use App\Repositories\QuestionRepository;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use DataTables;
use URL;
use Auth;
use Session;

class ReportController extends Controller
{
	protected $reportRps;
	protected $reportDetailRps;
	protected $crewRps;
	protected $questionRps;
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ReportRepository $reportRps, CrewRepository $crewRps, ReportDetailRepository $reportDetailRps, QuestionRepository $questionRps)
    { 
		$this->middleware('auth');
        $this->reportRps = $reportRps;
		$this->crewRps = $crewRps;
		$this->questionRps = $questionRps;
		$this->reportDetailRps = $reportDetailRps;
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('Superintendent.Report.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Crew  $crew
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		
        $info_Report = $this->reportRps->getCrewReport($id);
		$info_Question = $this->questionRps->getForemanQuestion($info_Report->foreman_id);
		return view('Superintendent.Report.show' ,array('info_Report' => $info_Report, 'info_Question' => $info_Question));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Crew  $crew
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		//base64_encode(3*25);
		if(substr($id,0,3)=="rpt")
		{
			$id = (base64_decode(substr($id,3))/25);
		}
		$info_Report = $this->reportRps->getCrewReport($id);
		$info_Crew = $this->crewRps->getCrew()->whereNotIn('id', $info_Report->Crew_report_detail->pluck('crew_id'));
		return view('Superintendent.Report.edit' ,array('info_Crew' => $info_Crew), compact('info_Report' , 'info_Report'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Crew  $crew
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		if(!isset($request["crew_id"]))
		{
			return view('Superintendent.Report.index');
		}
		//return $request->all();
		$this->reportRps->updateCrewReport($request->all(), $id);
		foreach($request["crew_id"] as $key => $crew_id)
		{
			$input = array("crew_id" => $crew_id, "report_id" => $id, "hours" => $request["hours"][$key]);
			if($request["id"][$key]==null)
			{
				$this->reportDetailRps->addCrewReportDetail($input);
			}
			else
			{
				$this->reportDetailRps->updateCrewReportDetail($input, $request["id"][$key]);
			}
		}
        Session::flash('flash_message', 'Crew Report successfully added!');
		return view('Superintendent.Report.index');
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   $info_CrewReport = $this->reportRps->getCrewReport();
	   return Datatables::of($info_CrewReport)
		->editColumn('id', function ($info_CrewReport) {
				 return '<a href="/Superintendent/Report/'.$info_CrewReport->id.'">'.$info_CrewReport->Foreman()->First()->name.'-'.$info_CrewReport->id.'</a>';
        })
		->editColumn('status', function ($info_CrewReport) {
				 if($info_CrewReport->status==0)
				 {
					 return "Pending";
				 }
				 else
				 if($info_CrewReport->status==1)
				 {
					return "Approve by Superintendent ";
				 }
        })
		->addColumn('edit', function ($info_CrewReport) {
				 if($info_CrewReport->status==0)
				 {
					 return '<div class="btn-group btn-group-action">
									<a class="btn btn-info" style="margin-right:2px;" href="'.url('/Superintendent/Report/'.$info_CrewReport->id.'/edit').'" title="Edit Data">Edit</i></a> 
							</div>
							<div class="btn-group btn-group-action">
									<a class="btn btn-success" href="javascript(0)" title="Approved Data" id="btnApproved" name="btnApproved" data-remote="/Superintendent/Report/Approved/' . $info_CrewReport->id . '">Approve</a>
							</div>';
				 }
				 else
				 {
					 return 'Approved';
				 }
        })
		->escapeColumns([])
		->removeColumn('foreman_id')
		->removeColumn('superintendent_id')
		->removeColumn('updated_at')
		->make(true);
    }
	
	public function ApprovedReport($ReportID)
	{
		$info_CrewReport = $this->reportRps->ApprovedReport($ReportID);
	}
}
