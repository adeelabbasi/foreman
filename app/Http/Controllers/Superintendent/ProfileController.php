<?php

namespace App\Http\Controllers\Superintendent;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use DataTables;
use URL;
use Auth;
use Session;

class ProfileController extends Controller
{
	protected $profileRps;
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $profileRps)
    { 
		$this->middleware('auth');
        $this->profileRps = $profileRps;
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$info_User = $this->profileRps->getUser("", Auth::user()->id);
		return view('Superintendent.Profile.edit',array('info_User' => $info_User));
    }
	
	public function show($id)
    {
		$info_User = $this->profileRps->getUser("", Auth::user()->id);
		return view('Superintendent.Profile.edit',array('info_User' => $info_User));
    }
	
    public function update(Request $request, $id)
    {
		$rules = [
            'name' => 'required|string|max:255',
            'email' =>  'unique:users,email,'.$id.'|required|email',
            'username' =>  'unique:users,username,'.$id.'|required|alpha_dash',
			'type' => 'string|max:1',
			'avatar' => 'image|mimes:jpg,png,jpeg|min:1|max:2048',
			'password' => 'nullable|string|min:6|confirmed',
        ];
		
		$validator = Validator::make($request->all(), $rules);
		if($validator->fails())
            return redirect()->back()->withErrors($validator)->withInput();
			
        $db_user = $this->profileRps->updateUser($request->all() , $id);
		
		return redirect('Superintendent/Profile');
    }
	
}
