<?php

namespace App\Http\Controllers\Foreman;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ReportRepository;
use App\Repositories\ReportDetailRepository;
use App\Repositories\CrewRepository;
use App\Repositories\QuestionRepository;
use Illuminate\Support\Facades\Validator;
use App\Mail\ReportMail;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use DataTables;
use URL;
use Auth;
use Session;

class ReportController extends Controller
{
	protected $reportRps;
	protected $reportDetailRps;
	protected $crewRps;
	protected $questionRps;
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ReportRepository $reportRps, CrewRepository $crewRps, ReportDetailRepository $reportDetailRps, QuestionRepository $questionRps)
    { 
		$this->middleware('auth');
        $this->reportRps = $reportRps;
		$this->crewRps = $crewRps;
		$this->questionRps = $questionRps;
		$this->reportDetailRps = $reportDetailRps;
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('Foreman.Report.index');
    }
	
	/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$info_ForemanCrew = $this->crewRps->getForemanCrew(Auth::user()->id);
		$info_Crew = $this->crewRps->getCrew()->whereNotIn('id', $info_ForemanCrew->pluck('id'));
		return view('Foreman.Report.create',array('info_Crew' => $info_Crew), compact('info_ForemanCrew' , 'info_ForemanCrew'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		if(!isset($request["crew_id"]))
		{
			return view('Foreman.Report.index');
		}
		$Report = $this->reportRps->addCrewReport($request->all());
		//Mail::to(Auth::User()->Superintendent()->First())->send(new ReportMail($Report));
		foreach($request["crew_id"] as $key => $crew_id)
		{
			$input = array("crew_id" => $crew_id, "report_id" => $Report->id, "hours" => $request["hours"][$key]);
			$this->reportDetailRps->addCrewReportDetail($input);
		}
		Session::flash('flash_message', 'Crew Report successfully added!');
		return view('Foreman.Report.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Crew  $crew
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$info_Question = $this->questionRps->getForemanQuestion(Auth::User()->id);
        $info_Report = $this->reportRps->getCrewReport($id);
		return view('Foreman.Report.show' ,array('info_Report' => $info_Report, 'info_Question' => $info_Question));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Crew  $crew
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		return 123;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Crew  $crew
     * @return \Illuminate\Http\Response
     */
    public function update(CrewRequest $request, $id)
    {
        return 123;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Crew  $crew
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return 123;
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   $info_CrewReport = $this->reportRps->getUserCrewReport(Auth::User()->id);
	   return Datatables::of($info_CrewReport)
		->editColumn('id', function ($info_CrewReport) {
				 return '<a href="/Foreman/Report/'.$info_CrewReport->id.'">'.$info_CrewReport->Foreman()->First()->name.'-'.$info_CrewReport->id.'</a>';
        })
		->editColumn('status', function ($info_CrewReport) {
				 if($info_CrewReport->status==0)
				 {
					 return "Submit";
				 }
				 else
				 if($info_CrewReport->status==1)
				 {
					return "Approve by Superintendent ";
				 }
        })
		/*->addColumn('edit', function ($info_CrewReport) {
				 return '<div class="btn-group btn-group-action">
								<a class="btn btn-info" style="margin-right:2px;" href="'.url('/Foreman/Report/'.$info_CrewReport->id.'/edit').'" title="Edit Data"><i class="fa fa-pencil"></i></a> 
								<a class="btn btn-danger" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-remote="/Foreman/Report/' . $info_CrewReport->id . '"><i class="fa fa-trash"></i></a>
						</div>';
        })*/
		->escapeColumns([])
		->removeColumn('foreman_id')
		->removeColumn('superintendent_id')
		->removeColumn('updated_at')
		->make(true);
    }
}
