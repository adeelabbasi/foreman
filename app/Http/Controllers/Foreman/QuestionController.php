<?php
namespace App\Http\Controllers\Foreman;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\QuestionRepository;
use App\Repositories\AnswerRepository;
use App\Models\Question;
use DataTables;
use URL;
use Auth;
use Session;

class QuestionController extends Controller
{
	protected $questionRps;
	protected $answerRps;
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(QuestionRepository $questionRps, AnswerRepository $answerRps)
    { 
		$this->middleware('auth');
        $this->questionRps = $questionRps;
		$this->answerRps = $answerRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Foreman.Question.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $info_Question = $this->questionRps->getQuestion($id);
		return view('Foreman.Question.show' ,array('info_Question' => $info_Question));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $info_Question = $this->questionRps->getQuestion($id);
		return view('Foreman.Question.edit' ,array('info_Question' => $info_Question));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$inputs = $request->all();
		$inputs["question_id"] = $id;
		if($inputs["answer_id"]=="")
		{
        	$this->answerRps->addAnswer($inputs);
		}
		else
		{
			$this->answerRps->updateAnswer($inputs,$inputs["answer_id"]);
		}
		Session::flash('flash_message', 'Answer successfully updated!');
		return view('Foreman.Question.index');
    }
	
	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function UpdateAnswer(Request $request)
    {
		$inputs = $request->all();
		if($inputs["answer_id"]=="")
		{
        	$this->answerRps->addAnswer($inputs);
		}
		else
		{
			$this->answerRps->updateAnswer($inputs,$inputs["answer_id"]);
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return back();
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   $info_Question = $this->questionRps->getForemanQuestion(Auth::User()->id);
	   return Datatables::of($info_Question)
		->editColumn('id', function ($info_Question) {
				return '<a href="/Foreman/Question/'.$info_Question->id.'">QUESTION-'.$info_Question->id.'</a>';
        })
		->editColumn('report_id', function ($info_Question) {
				
				if($info_Question->report_id!="")
					return '<a href="/Foreman/Report/'.$info_Question->Report()->First()->id.'">CREWREPORT-'.$info_Question->Report()->First()->id.'</a>';
				else
					return "";
        })
		->addColumn('answer', function ($info_Question) {
				 //if($info_Question->Answer()->Get()->Count()==0)
				 if(1==1)
				 {
					 return '<div class="row"><div class="col-md-12 col-sm-12 col-xs-12 form-group">
					 		<textarea id="answer'.$info_Question->id.'" data-question_id="'.$info_Question->id.'" data-user_id="'.$info_Question->user_id.'" class="form-control answerArea" cols="50" rows="4"></textarea>
							</div><div>';
					 
				 }
				 else
				 { 
				 	return '<div class="row"><div class="col-md-12 col-sm-12 col-xs-12 form-group">
					 		<textarea id="answer'.$info_Question->id.'" class="form-control" cols="50" rows="4">'.$info_Question->Answer()->First()->answer.'</textarea>
							</div><div>';
				 }
        })
		->addColumn('edit', function ($info_Question) {
				 if($info_Question->Answer()->Get()->Count()==0)
				 {
					 return '<div class="btn-group btn-group-action">
								<a class="btn btn-info" style="margin-right:2px;" id="btnUpdate" name="btnUpdate" data-question_id="'.$info_Question->id.'" data-answer_id="" data-user_id="'.$info_Question->user_id.'" data-question_id="" href="javascript:void(0)" title="Update Data">Update</a> 
						</div>';
					 
				 }
				 else
				 { 
				 	return '<div class="btn-group btn-group-action">
								<a class="btn btn-info" style="margin-right:2px;" id="btnUpdate" name="btnUpdate" data-question_id="'.$info_Question->id.'" data-answer_id="'.$info_Question->Answer()->First()->id.'" data-user_id="'.$info_Question->user_id.'" data-question_id="" href="javascript:void(0)" title="Update Data">Update</a> 
						</div>';
				 }
				 
        })
		->escapeColumns([])
		->removeColumn('user_id')
		->make(true);
    }
}
