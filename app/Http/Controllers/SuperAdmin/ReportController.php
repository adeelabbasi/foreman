<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ReportRepository;
use App\Repositories\ReportDetailRepository;
use App\Repositories\CrewRepository;
use App\Repositories\QuestionRepository;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use DataTables;
use URL;
use Auth;
use Session;

class ReportController extends Controller
{
	protected $reportRps;
	protected $reportDetailRps;
	protected $crewRps;
	protected $questionRps;
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ReportRepository $reportRps, CrewRepository $crewRps, ReportDetailRepository $reportDetailRps, QuestionRepository $questionRps)
    { 
		$this->middleware('auth');
        $this->reportRps = $reportRps;
		$this->crewRps = $crewRps;
		$this->questionRps = $questionRps;
		$this->reportDetailRps = $reportDetailRps;
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('SuperAdmin.Report.index');
    }
	
	/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$info_ForemanCrew = $this->crewRps->getForemanCrew(Auth::user()->id);
		$info_Crew = $this->crewRps->getCrew()->whereNotIn('id', $info_ForemanCrew->pluck('id'));
		return view('SuperAdmin.Report.create',array('info_Crew' => $info_Crew), compact('info_ForemanCrew' , 'info_ForemanCrew'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Crew  $crew
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		//$info_Question = $this->questionRps->getReportQuestion($id);
        $info_Report = $this->reportRps->getCrewReport($id);
		$info_Question = $this->questionRps->getForemanQuestion($info_Report->foreman_id);
		return view('SuperAdmin.Report.show' ,array('info_Report' => $info_Report, 'info_Question' => $info_Question));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Crew  $crew
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Crew  $crew
     * @return \Illuminate\Http\Response
     */
    public function update(CrewRequest $request, $id)
    {
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Crew  $crew
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return back();
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   $info_CrewReport = $this->reportRps->getCrewReport();
	   return Datatables::of($info_CrewReport)
		->editColumn('id', function ($info_CrewReport) {
				 return '<a href="/SuperAdmin/Report/'.$info_CrewReport->id.'">'.$info_CrewReport->Foreman()->First()->name.'-'.$info_CrewReport->id.'</a>';
        })
		->editColumn('status', function ($info_CrewReport) {
				 if($info_CrewReport->status==0)
				 {
					 return "Submit";
				 }
				 else
				 if($info_CrewReport->status==1)
				 {
					return "Approve by Superintendent ";
				 }
        })
		->editColumn('foreman_id', function ($info_CrewReport) {
				 if($info_CrewReport->foreman_id!="")
				 {
					 return $info_CrewReport->Foreman()->First()->name;
				 }
				 else
				 {
					return "None";
				 }
        })
		->editColumn('superintendent_id', function ($info_CrewReport) {
				 if($info_CrewReport->superintendent_id!="")
				 {
					 return $info_CrewReport->Superintendent()->First()->name;
				 }
				 else
				 {
					return "None";
				 }
        })
		->removeColumn('comments')
		->escapeColumns([])
		->make(true);
    }
}
