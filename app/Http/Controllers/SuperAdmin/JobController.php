<?php
namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\JobRepository;
use App\Http\Requests\JobRequest;
use App\Models\Job;
use DataTables;
use URL;
use Auth;
use Session;

class JobController extends Controller
{
	protected $jobRps;
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(JobRepository $jobRps)
    { 
		$this->middleware('auth');
        $this->jobRps = $jobRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('SuperAdmin.Job.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('SuperAdmin.Job.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$this->jobRps->addJob($request->all());
		Session::flash('flash_message', 'Job successfully added!');
		return view('SuperAdmin.Job.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $info_Job = $this->jobRps->getJob($id);
		return view('SuperAdmin.Job.edit' ,array('info_Job' => $info_Job));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $info_Job = $this->jobRps->getJob($id);
		return view('SuperAdmin.Job.edit' ,array('info_Job' => $info_Job));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function update(JobRequest $request, $id)
    {
        $this->jobRps->updateJob($request->all() , $id);
		Session::flash('flash_message', 'Job successfully updated!');
		return view('SuperAdmin.Job.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->jobRps->deleteJob($id);
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   $info_Job = $this->jobRps->getJob();
	   return Datatables::of($info_Job)
		->addColumn('edit', function ($info_Job) {
				 return '<div class="btn-group btn-group-action">
								<a class="btn btn-info" style="margin-right:2px;" href="'.url('/SuperAdmin/Job/'.$info_Job->id.'/edit').'" title="Edit Data"><i class="fa fa-pencil"></i></a> 
								<a class="btn btn-danger" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-remote="/SuperAdmin/Job/' . $info_Job->id . '"><i class="fa fa-trash"></i></a>
						</div>';
        })
		->editColumn('id', 'JOB-{{ $id }}')
		->escapeColumns([])
		->make(true);
    }
}
