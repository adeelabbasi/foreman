<?php
namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CrewRepository;
use App\Http\Requests\CrewRequest;
use App\Repositories\UserRepository;
use App\Repositories\JobRepository;
use App\Models\Crew;
use DataTables;
use URL;
use Auth;
use Session;

class CrewController extends Controller
{
	protected $crewRps;
	protected $foremanRps;
	protected $jobRps;
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CrewRepository $crewRps, UserRepository $foremanRps, JobRepository $jobRps)
    { 
		$this->middleware('auth');
        $this->crewRps = $crewRps;
		$this->foremanRps = $foremanRps;
		$this->jobRps = $jobRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('SuperAdmin.Crew.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$info_Job = $this->jobRps->getJob();
		$info_Foreman = $this->foremanRps->getUser(3);
        return view('SuperAdmin.Crew.add', compact('info_Job','info_Job'), compact('info_Foreman','info_Foreman'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		foreach($request["name"] as $key => $name)
		{
			$input = array("name" => $name, "user_id" => $request["user_id"][$key], "job_id" => $request["job_id"][$key]);
			$this->crewRps->addCrew($input);
		}
		Session::flash('flash_message', 'Crew successfully added!');
		return view('SuperAdmin.Crew.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Crew  $crew
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $info_Crew = $this->crewRps->getCrew($id);
		return view('SuperAdmin.Crew.edit' ,array('info_Crew' => $info_Crew));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Crew  $crew
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$info_Job = $this->jobRps->getJob();
		$info_Foreman = $this->foremanRps->getUser(3);
        $info_Crew = $this->crewRps->getCrew($id);
		return view('SuperAdmin.Crew.edit' ,array('info_Crew' => $info_Crew, 'info_Foreman' => $info_Foreman, 'info_Job' => $info_Job));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Crew  $crew
     * @return \Illuminate\Http\Response
     */
    public function update(CrewRequest $request, $id)
    {
        $this->crewRps->updateCrew($request->all() , $id);
		Session::flash('flash_message', 'Crew successfully updated!');
		return view('SuperAdmin.Crew.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Crew  $crew
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->crewRps->deleteCrew($id);
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   $info_Crew = $this->crewRps->getCrew();
	   return Datatables::of($info_Crew)
	   	->editColumn('user_id', function ($info_Crew) {
				 if($info_Crew->user_id==0)
				 {
					 return "None";
				 }
				 else
				 {
					return '<a href="/SuperAdmin/Foreman/'.$info_Crew->User()->First()->id.'">'.$info_Crew->User()->First()->name.'</a>';
				 }
        })
		->editColumn('id', 'CREW-{{ $id }}')
		->editColumn('job_id', function ($info_Crew) {
			return $info_Crew->Job()->First()->title;
        })
		->addColumn('edit', function ($info_Crew) {
				 return '<div class="btn-group btn-group-action">
								<a class="btn btn-info" style="margin-right:2px;" href="'.url('/SuperAdmin/Crew/'.$info_Crew->id.'/edit').'" title="Edit Data"><i class="fa fa-pencil"></i></a> 
								<a class="btn btn-danger" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-remote="/SuperAdmin/Crew/' . $info_Crew->id . '"><i class="fa fa-trash"></i></a>
						</div>';
        })
		->escapeColumns([])
		->removeColumn('type')
		->make(true);
    }
}
