<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    protected $profileRps;
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $profileRps)
    { 
		$this->middleware('auth');
        $this->profileRps = $profileRps;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $info_User = $this->profileRps->getUser("", Auth::user()->id);
		return view('SuperAdmin.home',array('info_User' => $info_User));
    }
}
