<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Repositories\JobRepository;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use DataTables;
use URL;
use Auth;
use Session;

class SuperintendentController extends Controller
{
	protected $superintendentRps;
	protected $jobRps;
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $superintendentRps, JobRepository $jobRps)
    { 
		$this->middleware('auth');
        $this->superintendentRps = $superintendentRps;
		$this->jobRps = $jobRps;
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('SuperAdmin.Superintendent.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$info_Job = $this->jobRps->getJob();
        return view('SuperAdmin.Superintendent.add', compact('info_Job','info_Job'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $this->superintendentRps->addUser($request->all());
		Session::flash('flash_message', 'Superintendent successfully added!');
		return view('SuperAdmin.Superintendent.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Superintendent  $superintendent
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $info_Superintendent = $this->superintendentRps->getUser(2, $id);
		return view('SuperAdmin.Superintendent.show' ,array('info_Superintendent' => $info_Superintendent));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Superintendent  $superintendent
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$info_Job = $this->jobRps->getJob();
        $info_Superintendent = $this->superintendentRps->getUser(2, $id);
		return view('SuperAdmin.Superintendent.edit' ,array('info_Superintendent' => $info_Superintendent), compact('info_Job','info_Job'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Superintendent  $superintendent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$rules = [
            'name' => 'required|string|max:255',
            'email' =>  'unique:users,email,'.$id.'|required|email',
            'username' =>  'unique:users,username,'.$id.'|required|alpha_dash',
			'type' => 'string|max:1',
        ];
		
		$validator = Validator::make($request->all(), $rules);
		if($validator->fails())
            return redirect()->back()->withErrors($validator)->withInput();
		
        $this->superintendentRps->updateUser($request->all() , $id);
		Session::flash('flash_message', 'Superintendent successfully updated!');
		return view('SuperAdmin.Superintendent.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Superintendent  $superintendent
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->superintendentRps->deleteUser($id);
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   $info_Superintendent = $this->superintendentRps->getUser(2);
	   return Datatables::of($info_Superintendent)
	    ->editColumn('name', '<a href="/SuperAdmin/Superintendent/{{$id}}">{{ $name }}</a>')
		->addColumn('edit', function ($info_Superintendent) {
				 return '<div class="btn-group btn-group-action">
								<a class="btn btn-info" style="margin-right:2px;" href="'.url('/SuperAdmin/Superintendent/'.$info_Superintendent->id.'/edit').'" title="Edit Data"><i class="fa fa-pencil"></i></a> 
								<a class="btn btn-danger" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-remote="/SuperAdmin/Superintendent/' . $info_Superintendent->id . '"><i class="fa fa-trash"></i></a>
						</div>';
        })
		->editColumn('id', 'SUP-{{ $id }}')
		->escapeColumns([])
		->removeColumn('type')
		->make(true);
    }
}
