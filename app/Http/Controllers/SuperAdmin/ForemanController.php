<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Repositories\JobRepository;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use DataTables;
use URL;
use Auth;
use Session;

class ForemanController extends Controller
{
	protected $foremanRps;
	protected $jobRps;
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $foremanRps, JobRepository $jobRps)
    { 
		$this->middleware('auth');
        $this->foremanRps = $foremanRps;
		$this->jobRps = $jobRps;
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('SuperAdmin.Foreman.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$info_Superintendent = $this->foremanRps->getUser(2);
		$info_Job = $this->jobRps->getJob();
        return view('SuperAdmin.Foreman.add', compact('info_Job','info_Job'),compact('info_Superintendent','info_Superintendent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
		//return $request->all();
        $this->foremanRps->addUser($request->all());
		Session::flash('flash_message', 'Foreman successfully added!');
		return view('SuperAdmin.Foreman.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Foreman  $foreman
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $info_Foreman = $this->foremanRps->getUser(3, $id);
		return view('SuperAdmin.Foreman.show' ,array('info_Foreman' => $info_Foreman));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Foreman  $foreman
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$info_Superintendent = $this->foremanRps->getUser(2);
		$info_Job = $this->jobRps->getJob();
        $info_Foreman = $this->foremanRps->getUser(3, $id);
		return view('SuperAdmin.Foreman.edit' ,array('info_Foreman' => $info_Foreman, 'info_Job' => $info_Job, 'info_Superintendent' => $info_Superintendent));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Foreman  $foreman
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$rules = [
            'name' => 'required|string|max:255',
            'email' =>  'unique:users,email,'.$id.'|required|email',
            'username' =>  'unique:users,username,'.$id.'|required|alpha_dash',
			'type' => 'string|max:1',
        ];
		$validator = Validator::make($request->all(), $rules);
		if($validator->fails())
            return redirect()->back()->withErrors($validator)->withInput();
		
        $this->foremanRps->updateUser($request->all() , $id);
		Session::flash('flash_message', 'Foreman successfully updated!');
		return view('SuperAdmin.Foreman.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Foreman  $foreman
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->foremanRps->deleteUser($id);
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   $info_Foreman = $this->foremanRps->getUser(3);
	   return Datatables::of($info_Foreman)
	    ->editColumn('name', '<a href="/SuperAdmin/Foreman/{{$id}}">{{ $name }}</a>')
		->addColumn('edit', function ($info_Foreman) {
				 return '<div class="btn-group btn-group-action">
								<a class="btn btn-info" style="margin-right:2px;" href="'.url('/SuperAdmin/Foreman/'.$info_Foreman->id.'/edit').'" title="Edit Data"><i class="fa fa-pencil"></i></a> 
								<a class="btn btn-danger" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-remote="/SuperAdmin/Foreman/' . $info_Foreman->id . '"><i class="fa fa-trash"></i></a>
						</div>';
        })
		->editColumn('id', 'SUP-{{ $id }}')
		->escapeColumns([])
		->removeColumn('type')
		->make(true);
    }
}
