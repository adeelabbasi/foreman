<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['prefix' => 'SuperAdmin', 'middleware' => ['auth', 'authType:1']], function() {
	Route::resource('/', 'SuperAdmin\HomeController');	
	
	Route::get('/Superintendent/grid', 'SuperAdmin\SuperintendentController@grid');
	Route::resource('/Superintendent', 'SuperAdmin\SuperintendentController');	
	
	Route::get('/Foreman/grid', 'SuperAdmin\ForemanController@grid');
	Route::resource('/Foreman', 'SuperAdmin\ForemanController');	
	
	Route::get('/Crew/grid', 'SuperAdmin\CrewController@grid');
	Route::resource('/Crew', 'SuperAdmin\CrewController');	
	
	Route::get('/Job/grid', 'SuperAdmin\JobController@grid');
	Route::resource('/Job', 'SuperAdmin\JobController');	
	
	Route::get('/Report/grid', 'SuperAdmin\ReportController@grid');
	Route::resource('/Report', 'SuperAdmin\ReportController');
	
	Route::get('/Question/grid', 'SuperAdmin\QuestionController@grid');
	Route::resource('/Question', 'SuperAdmin\QuestionController');
	
	Route::resource('/Profile', 'SuperAdmin\ProfileController');
});

Route::group(['prefix' => 'Superintendent', 'middleware' => ['auth', 'authType:2']], function() {
	Route::resource('/', 'Superintendent\HomeController');	
	
	Route::resource('/Profile', 'Superintendent\ProfileController');
	
	Route::post('/Report/Approved/{ReportID}', 'Superintendent\ReportController@ApprovedReport');
	Route::get('/Report/grid', 'Superintendent\ReportController@grid');
	Route::resource('/Report', 'Superintendent\ReportController');
	
	Route::post('/Question/UpdateAnswer', 'Superintendent\QuestionController@UpdateAnswer');
	Route::get('/Question/grid', 'Superintendent\QuestionController@grid');
	Route::resource('/Question', 'Superintendent\QuestionController');
});

Route::group(['prefix' => 'Foreman', 'middleware' => ['auth', 'authType:3']], function() {
	Route::resource('/', 'Foreman\HomeController');	
	
	Route::resource('/Profile', 'Foreman\ProfileController');
	
	Route::get('/Report/grid', 'Foreman\ReportController@grid');
	Route::resource('/Report', 'Foreman\ReportController');
	
	Route::post('/Question/UpdateAnswer', 'Foreman\QuestionController@UpdateAnswer');
	Route::get('/Question/grid', 'Foreman\QuestionController@grid');
	Route::resource('/Question', 'Foreman\QuestionController');
});
