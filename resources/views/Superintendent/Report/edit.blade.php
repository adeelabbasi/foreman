@extends('layouts.app')
@section('content')
<div class="page-title">
  <div class="title_left">
    <h3>Superintendent Report <small></small></h3>
  </div>

  <div class="title_right">
    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
      <div class="input-group pull-right">
        <a href="/{{ Auth::User()->getRoleNames()[0] }}"><i class="fa fa-home"></i> Dashboard</a> > <a href="/{{ Auth::User()->getRoleNames()[0] }}/Report"><i class="fa fa-users"></i> Report</a>
      </div>
    </div>
  </div>
</div>
<div class="row">
   <div class="col-md-8 col-xs-12">
      <div class="x_panel">
         <div class="x_title">
            <h2>Submit Report</h2>
            <div class="clearfix"></div>
         </div>
         <div class="x_content">
            <br>
            <div class="row">
                <div class="col-md-5 col-sm-5 col-xs-5 form-group">
                  <label>Add New Crew</label>
			  	  <?php
                    $defaultSelection = ['0'=>'Please Select'];
                    foreach($info_Crew as $selectItem)
                    {
                        $defaultSelection = $defaultSelection +  array($selectItem->id => ($selectItem->name));
                    }
                  ?>
                  {!! Form::select('newcrews', $defaultSelection, null, ['class' => 'form-control', 'id' => 'newcrews']) !!}
               </div>
               <div class="col-md-3 col-sm-3 col-xs-3 form-group">
                   <label>&nbsp;</label><br />
                  <a href="javascript:void(0)" class="btn btn-success addnew"><i class="fa fa-plus" aria-hidden="true"></i></a>
               </div>
           </div>
           <hr />
           {!! Form::model($info_Report, ['method' => 'PATCH', 'url' => ['/Superintendent/Report', $info_Report->id], 'files' => true,'id' => 'main-form']) !!}
           	   <input type="hidden" name="status" value="1" />
           	   <input type="hidden" name="foreman_id" value="{{ $info_Report->foreman_id }}" />
               <input type="hidden" name="superintendent_id" value="{{ Auth::user()->id }}" />
               <table id="viewForm" class="table table-striped table-bordered bulk_action">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Hours</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  	@foreach($info_Report->Crew_report_detail()->Get() as $ReportDetail)
                  	<tr>
                      <td>
                      	CREW-{{ $ReportDetail->crew_id }}
                        {!! Form::hidden('id[]', $ReportDetail->id, []) !!}
                        {!! Form::hidden('crew_id[]', $ReportDetail->crew_id, []) !!}
                      </td>
                      <td>{{ $ReportDetail->Crew()->First()->name }}</td>
                      <td>{!! Form::text('hours[]', $ReportDetail->hours, ['class' => 'form-control' , 'required' , 'placeholder' => 'Hours', 'id' => 'hours']) !!}</td>
                      <td></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
               <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                        <label>Comments:</label>
                        <textarea name="comments" class="form-control"></textarea>
                   </div>
               </div>
               <div class="ln_solid"></div>
               <div class="form-group">
                  <div class="col-md-9 col-sm-9 col-xs-12">
                     <button type="submit" class="btn btn-success">Submit</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection
@push('scripts')

<script type="text/javascript">
	$(document).on('click', '.addnew', function () {
		
		if($("#newcrews").val()!=0)
		{
			var id = $("#newcrews").val();
			var text = $("#newcrews").children(':selected').text();
			var html = '<tr><td>CREW-'+id+'<input type="hidden" id="id" name="id[]" value=""><input type="hidden" id="crew_id" name="crew_id[]" value="'+id+'"></td><td>'+text+'</td><td><input class="form-control" required="" placeholder="Hours" id="hours" name="hours[]" type="text"></td><td><a href="javascript:void(0)" data-id="'+id+'" data-text="'+text+'" class="btn btn-warning remove"><i class="fa fa-minus" aria-hidden="true"></i></a></td></tr>';
		   $("#viewForm tbody").append(html);
		   $('#newcrews option[value="'+id+'"]').remove();
		}
	});
	
	$(document).on('click', '.remove', function () {
		var id = $(this).data("id");
		var text = $(this).data("text");
		$(this).parents("tr").remove();
		$("#newcrews").append('<option value="'+id+'">'+text+'</option>');
	});
	
	


</script>

@endpush
