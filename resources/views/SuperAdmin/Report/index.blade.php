@extends('layouts.app')


@section('content')
<div class="page-title">
  <div class="title_left">
    <h3>Crew Report <small></small></h3>
  </div>

  <div class="title_right">
    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
      <div class="input-group pull-right">
        <a href="/{{ Auth::User()->getRoleNames()[0] }}"><i class="fa fa-home"></i> Dashboard</a>
      </div>
    </div>
  </div>
</div>

<div class="clearfix"></div>
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="x_panel">
  <div class="x_title">
    <h2>Crew Reports</small></h2>
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
    <table id="viewForm" class="table table-striped table-bordered bulk_action">
      <thead>
        <tr>
          <th>Name</th>
		  <th>Superintendent</th>
          <th>Foreman</th>
          <th>Status</th>
          <th>Create Date</th>
          <th>Update Date</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
</div>
</div>

@endsection

@push('scripts')

<script type="text/javascript">
    $('#viewForm').DataTable({
        "processing": true,
        "serverSide": true,
		"ajax": "{{url('SuperAdmin/Report/grid')}}",
        "columns": [
			{ data: 'id', name: 'id' },
			{ data: 'superintendent_id', name: 'superintendent_id' },
			{ data: 'foreman_id', name: 'foreman_id' },
			{ data: 'status', name: 'status' },
			{ data: 'created_at', name: 'created_at' },
			{ data: 'updated_at', name: 'updated_at' },
		],
		"responsive": true,
		dom: 'Bfrtip',
		buttons: [
			'copy', 'csv'
		],
		order: [ [0, 'desc'] ]
    });
</script>

@endpush