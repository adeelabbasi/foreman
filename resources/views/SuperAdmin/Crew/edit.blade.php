@extends('layouts.app')
@section('content')
<div class="page-title">
  <div class="title_left">
    <h3>Crew <small></small></h3>
  </div>

  <div class="title_right">
    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
      <div class="input-group pull-right">
        <a href="/{{ Auth::User()->getRoleNames()[0] }}"><i class="fa fa-home"></i> Dashboard</a> > <a href="/{{ Auth::User()->getRoleNames()[0] }}/Crew"><i class="fa fa-users"></i> Crew</a>
      </div>
    </div>
  </div>
</div>
<div class="row">
   <div class="col-md-6 col-xs-12">
      <div class="x_panel">
         <div class="x_title">
            <h2>Add  Crew </h2>
            <div class="clearfix"></div>
         </div>
         <div class="x_content">
            <br>
            {!! Form::model($info_Crew, ['method' => 'PATCH', 'url' => ['/SuperAdmin/Crew', $info_Crew->id], 'files' => true,'id' => 'main-form']) !!}
               	<div class="col-md-12 col-sm-12 col-xs-12 form-group">
                  <label for="exampleInputEmail1">Name</label>
                  {!! Form::text('name', null, ['class' => 'form-control' , 'required' , 'placeholder' => 'Name', 'id' => 'name']) !!}
                  @if ($errors->has('name'))<p style="color:red;">{!!$errors->first('name')!!}</p>@endif
               </div>
               <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <?php
					$defaultSelection = ['0'=>'Please Select'];
					foreach($info_Foreman as $selectItem)
					{
						$defaultSelection = $defaultSelection +  array($selectItem->id => ($selectItem->name));
					}
				  ?>
				  {!! Form::select('user_id', $defaultSelection, null, ['class' => 'form-control']) !!}
				  @if ($errors->has('user_id'))<p style="color:red;">{!!$errors->first('user_id')!!}</p>@endif
               </div>
               <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                  <label for="exampleInputEmail1">Job</label>
                  <?php
					$defaultSelection = [];
					foreach($info_Job as $selectItem)
					{
						$defaultSelection = $defaultSelection +  array($selectItem->id => ($selectItem->title));
					}
				  ?>
				  {!! Form::select('job_id', $defaultSelection, null, ['class' => 'form-control']) !!}
				  @if ($errors->has('job_id'))<p style="color:red;">{!!$errors->first('job_id')!!}</p>@endif
               </div>
               <div class="ln_solid"></div>
               <div class="form-group">
                  <div class="col-md-9 col-sm-9 col-xs-12">
                     <button type="submit" class="btn btn-success">Submit</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection
