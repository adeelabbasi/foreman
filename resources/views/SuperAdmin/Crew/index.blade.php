@extends('layouts.app')


@section('content')
<div class="page-title">
  <div class="title_left">
    <h3>Crew <small></small></h3>
  </div>

  <div class="title_right">
    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
      <div class="input-group pull-right">
        <a href="/{{ Auth::User()->getRoleNames()[0] }}"><i class="fa fa-home"></i> Dashboard</a>
      </div>
    </div>
  </div>
</div>

<div class="clearfix"></div>
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="x_panel">
  <div class="x_title">
    <h2>Crew Detail</small></h2>
    <ul class="nav navbar-right panel_toolbox">
      <li><a href="{{ url('/SuperAdmin/Crew/create') }}"><i class="fa fa-plus"></i> Add New</a></li>
      </li>
    </ul>
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
    <table id="viewForm" class="table table-striped table-bordered bulk_action">
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Foreman</th>
          <th>Job</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
</div>
</div>

@endsection

@push('scripts')

<script type="text/javascript">
    $('#viewForm').DataTable({
        "processing": true,
        "serverSide": true,
		"ajax": "{{url('/SuperAdmin/Crew/grid')}}",
        "columns": [
			{ data: 'id', name: 'id' },
			{ data: 'name', name: 'name' },
            { data: 'user_id', name: 'user_id' },
			{ data: 'job_id', name: 'job_id' },
			{ data: 'edit', name: 'edit', orderable: false, searchable: false }
		],
		"responsive": true,
		dom: 'Bfrtip',
		buttons: [
			'copy', 'csv'
		],
		order: [ [0, 'desc'] ]
    });
	
	$('#viewForm').on('click', '#btnDelete[data-remote]', function (e) { 
		if (confirm("Are you sure to delete crew?")) {		
			e.preventDefault();		 
			var url = '{{url("/")}}'+$(this).data('remote');
			// confirm then
			$.ajax({
				url: url,
				type: 'DELETE',
				dataType: 'json',
				data: {method: '_DELETE', "_token": "{{ csrf_token() }}" , submit: true},
				error: function (result, status, err) {
					//alert(result.responseText);
					//alert(status.responseText);
					//alert(err.Message);
				},
			}).always(function (data) {
				$('#viewForm').DataTable().draw(false);
			});
		}
		return false;
	});

</script>

@endpush