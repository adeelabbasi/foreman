@extends('layouts.app')
@section('content')
<div class="page-title">
  <div class="title_left">
    <h3>Crew <small></small></h3>
  </div>

  <div class="title_right">
    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
      <div class="input-group pull-right">
        <a href="/{{ Auth::User()->getRoleNames()[0] }}"><i class="fa fa-home"></i> Dashboard</a> > <a href="/{{ Auth::User()->getRoleNames()[0] }}/Crew"><i class="fa fa-users"></i> Crew</a>
      </div>
    </div>
  </div>
</div>
<div class="row">
   <div class="col-md-12 col-xs-12">
      <div class="x_panel">
         <div class="x_title">
            <h2>Add  Crew </h2>
            <div class="clearfix"></div>
         </div>
         <div class="x_content">
            <br>
            {!! Form::open([ 'url' => '/SuperAdmin/Crew/', 'files' => true, 'id' => 'main-form' ]) !!}
               	<div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-3 form-group">
                      <label>Name</label>
                      {!! Form::text('name[]', null, ['class' => 'form-control' , 'required' , 'placeholder' => 'Name', 'id' => 'name']) !!}
                      @if ($errors->has('name'))<p style="color:red;">{!!$errors->first('name')!!}</p>@endif
                   </div>
                   <div class="col-md-3 col-sm-3 col-xs-3 form-group">
                      <label>Foreman</label>
					  <?php
						$defaultSelection = ['0'=>'Please Select'];
						foreach($info_Foreman as $selectItem)
						{
							$defaultSelection = $defaultSelection +  array($selectItem->id => ($selectItem->name));
						}
                      ?>
                      {!! Form::select('user_id[]', $defaultSelection, null, ['class' => 'form-control']) !!}
                      @if ($errors->has('user_id'))<p style="color:red;">{!!$errors->first('user_id')!!}</p>@endif
                   </div>
                   <div class="col-md-3 col-sm-3 col-xs-3 form-group">
                      <label>Job</label>
					  <?php
						$defaultSelection = [];
						foreach($info_Job as $selectItem)
						{
							$defaultSelection = $defaultSelection +  array($selectItem->id => ($selectItem->title));
						}
                      ?>
                      {!! Form::select('job_id[]', $defaultSelection, null, ['class' => 'form-control']) !!}
                      @if ($errors->has('job_id'))<p style="color:red;">{!!$errors->first('job_id')!!}</p>@endif
                   </div>
                   
                   <div class="col-md-3 col-sm-3 col-xs-3 form-group">
                   	  <label>&nbsp;</label><br />
                   	  <a href="javascript:void(0)" class="btn btn-success addnew"><i class="fa fa-plus" aria-hidden="true"></i></a>
                   </div>
               </div>
               <div id="new"></div>
               <div class="ln_solid"></div>
               <div class="form-group">
                  <div class="col-md-9 col-sm-9 col-xs-12">
                     <button type="submit" class="btn btn-success">Submit</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>

<div id="copy" style="display:none">
	<div class="row">
        <div class="col-md-3 col-sm-3 col-xs-3 col-xs-5 form-group">
          <label>Name</label>
          {!! Form::text('name[]', null, ['class' => 'form-control' , 'required' , 'placeholder' => 'Name', 'id' => 'name']) !!}
          @if ($errors->has('name'))<p style="color:red;">{!!$errors->first('name')!!}</p>@endif
       </div>
       <div class="col-md-3 col-sm-3 col-xs-3 form-group">
          <label>Foreman</label>
          <?php
            $defaultSelection = ['0'=>'Please Select'];
            foreach($info_Foreman as $selectItem)
            {
                $defaultSelection = $defaultSelection +  array($selectItem->id => ($selectItem->name));
            }
          ?>
          {!! Form::select('user_id[]', $defaultSelection, null, ['class' => 'form-control']) !!}
          @if ($errors->has('user_id'))<p style="color:red;">{!!$errors->first('user_id')!!}</p>@endif
       </div>
      <div class="col-md-3 col-sm-3 col-xs-3 form-group">
          <label>Job</label>
          <?php
            $defaultSelection = [];
            foreach($info_Job as $selectItem)
            {
                $defaultSelection = $defaultSelection +  array($selectItem->id => ($selectItem->title));
            }
          ?>
          {!! Form::select('job_id[]', $defaultSelection, null, ['class' => 'form-control']) !!}
          @if ($errors->has('job_id'))<p style="color:red;">{!!$errors->first('job_id')!!}</p>@endif
       </div>
       <div class="col-md-3 col-sm-3 col-xs-3 form-group">
          <label>&nbsp;</label><br />
          <a href="javascript:void(0)" class="btn btn-warning remove"><i class="fa fa-minus" aria-hidden="true"></i></a>
       </div>
   </div>
</div>
@endsection
@push('scripts')

<script type="text/javascript">
	$(document).on('click', '.addnew', function () {
		var copy = $('#copy').html();
		$('#new').append(copy);
		return false;
	});
	
	$(document).on('click', '.remove', function () {
		$(this).parent().parent().remove();
	});



</script>

@endpush
