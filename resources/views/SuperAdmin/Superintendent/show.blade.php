@extends('layouts.app')

@section('content')
<div class="page-title">
  <div class="title_left">
    <h3>Superintendent <small></small></h3>
  </div>

  <div class="title_right">
    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
      <div class="input-group pull-right">
        <a href="/{{ Auth::User()->getRoleNames()[0] }}"><i class="fa fa-home"></i> Dashboard</a> > <a href="/{{ Auth::User()->getRoleNames()[0] }}/Superintendent"><i class="fa fa-user-secret"></i> Superintendent</a>
      </div>
    </div>
  </div>
</div>

<div class="clearfix"></div>
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="x_panel">
  <div class="x_title">
    <h2>Superintendent Detail</small></h2>
    <ul class="nav navbar-right panel_toolbox">
      <li><a href="{{ url('/SuperAdmin/Superintendent/create') }}"><i class="fa fa-plus"></i> Add New</a></li>
      </li>
    </ul>
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
    <table id="viewForm" class="table table-striped table-bordered bulk_action">
      <thead>
        <tr>
          <th>Name</th>
          <th>Username</th>
          <th>Email</th>
          <th>Job</th>
          <!--<th>Avatar</th>-->
        </tr>
      </thead>
      <tbody>
      	<td>{{ $info_Superintendent->name }} </td>
        <td>{{ $info_Superintendent->username }}</td>
        <td>{{ $info_Superintendent->email }}</td>
        <td>
        	@if($info_Superintendent->job_id!="")
        		{{ $info_Superintendent->Job()->First()->title }}
            @endif
        </td>
        <!--<td>{{ $info_Superintendent->avatar }}</td>-->
      </tbody>
      <tbody>
      </tbody>
    </table>
  </div>
</div>
</div>
@endsection
