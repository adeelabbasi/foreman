@extends('layouts.app')
@section('content')
<div class="page-title">
  <div class="title_left">
    <h3>Superintendent <small></small></h3>
  </div>

  <div class="title_right">
    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
      <div class="input-group pull-right">
        <a href="/{{ Auth::User()->getRoleNames()[0] }}"><i class="fa fa-home"></i> Dashboard</a> > <a href="/{{ Auth::User()->getRoleNames()[0] }}/Superintendent"><i class="fa fa-user-secret"></i> Superintendent</a>

      </div>
    </div>
  </div>
</div>
<div class="row">
   <div class="col-md-6 col-xs-12">
      <div class="x_panel">
         <div class="x_title">
            <h2>Add  Superintendent </h2>
            <div class="clearfix"></div>
         </div>
         <div class="x_content">
            <br>
            {!! Form::open([ 'url' => '/SuperAdmin/Superintendent/', 'files' => true, 'id' => 'main-form' ]) !!}
            	<input type="hidden" name="type" value="2" />
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <label for="exampleInputEmail1">Name</label>
                      {!! Form::text('name', null, ['class' => 'form-control' , 'required' , 'placeholder' => 'Name', 'id' => 'name']) !!}
                      @if ($errors->has('name'))<p style="color:red;">{!!$errors->first('name')!!}</p>@endif
                   </div>
                   <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <label for="exampleInputEmail1">Username</label>
                      {!! Form::text('username', null, ['class' => 'form-control' , 'required' , 'placeholder' => 'username', 'id' => 'username']) !!}
                      @if ($errors->has('username'))<p style="color:red;">{!!$errors->first('username')!!}</p>@endif
                   </div>
                   <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <label>Job</label>
					  <?php
						$defaultSelection = [];
						foreach($info_Job as $selectItem)
						{
							$defaultSelection = $defaultSelection +  array($selectItem->id => ($selectItem->title));
						}
                      ?>
                      {!! Form::select('job_id', $defaultSelection, null, ['class' => 'form-control']) !!}
                      @if ($errors->has('job_id'))<p style="color:red;">{!!$errors->first('job_id')!!}</p>@endif
                   </div>
                   <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <label for="exampleInputEmail1">Email</label>
                      {!! Form::text('email', null, ['class' => 'form-control' , 'required' , 'placeholder' => 'Email', 'id' => 'email']) !!}
                      @if ($errors->has('email'))<p style="color:red;">{!!$errors->first('email')!!}</p>@endif
                   </div>
                   <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <label for="exampleInputEmail1">Password</label>
                      {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'password']) !!}
                      @if ($errors->has('password'))<p style="color:red;">{!!$errors->first('password')!!}</p>@endif
                   </div>
                   <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <label for="exampleInputEmail1">Confirm Password</label>
                      {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'confirm password']) !!}
                      @if ($errors->has('password_confirmation'))<p style="color:red;">{!!$errors->first('password_confirmation')!!}</p>@endif
                   </div>
               </div>
               <div class="ln_solid"></div>
               <div class="form-group">
                  <div class="col-md-9 col-sm-9 col-xs-12">
                     <button type="submit" class="btn btn-success">Submit</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection
