@extends('layouts.app')
@section('content')
<div class="page-title">
  <div class="title_left">
    <h3>Question <small></small></h3>
  </div>

  <div class="title_right">
    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
      <div class="input-group pull-right">
        <a href="/{{ Auth::User()->getRoleNames()[0] }}"><i class="fa fa-home"></i> Dashboard</a> > <a href="/{{ Auth::User()->getRoleNames()[0] }}/Question"><i class="fa fa-users"></i> Question</a>
      </div>
    </div>
  </div>
</div>
<div class="row">
   <div class="col-md-6 col-xs-12">
      <div class="x_panel">
         <div class="x_title">
            <h2>Add  Answer </h2>
            <div class="clearfix"></div>
         </div>
         <div class="x_content">
            <br>
            {!! Form::model($info_Question, ['method' => 'PATCH', 'url' => ['/Foreman/Question', $info_Question->id], 'files' => true,'id' => 'main-form']) !!}
            	<input type="hidden" name="user_id" value="{{ $info_Question->user_id }}" />
               	<div class="col-md-12 col-sm-12 col-xs-12 form-group">
                  <label>Question</label>
                  <span class="form-control">{{ $info_Question->question }}</span>
               </div>
               <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                  <label>Answer</label>
				  {!! Form::textarea('answer', '' , ['class' => 'form-control']) !!}
				  @if ($errors->has('answer'))<p style="color:red;">{!!$errors->first('answer')!!}</p>@endif
               </div>
               <div class="ln_solid"></div>
               <div class="form-group">
                  <div class="col-md-9 col-sm-9 col-xs-12">
                     <button type="submit" class="btn btn-success">Submit</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection
