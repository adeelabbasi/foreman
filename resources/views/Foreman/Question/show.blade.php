@extends('layouts.app')
@section('content')
<div class="page-title">
  <div class="title_left">
    <h3>Question <small></small></h3>
  </div>

  <div class="title_right">
    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
      <div class="input-group pull-right">
        <a href="/{{ Auth::User()->getRoleNames()[0] }}"><i class="fa fa-home"></i> Dashboard</a> > <a href="/{{ Auth::User()->getRoleNames()[0] }}/Report"><i class="fa fa-users"></i> Report</a>
      </div>
    </div>
  </div>
</div>
<div class="row">
   <div class="col-md-8 col-xs-12">
      <div class="x_panel">
         <div class="x_title">
            <h2>View Question</h2>
            <div class="clearfix"></div>
         </div>
         <div class="x_content">
            <br>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                  <label>Question ID: </label>
			  	  QUESTION-{{ $info_Question->id }}
               </div>
               <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                  <label>Foreman: </label>
			  	  {{ $info_Question->User()->First()->name }}
               </div>
           </div>
           <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                  <label>Question: </label>
			  	  {{ $info_Question->question }}
               </div>
           </div>
           <hr />
           <table id="viewForm" class="table table-striped table-bordered bulk_action">
              <thead>
                <tr>
                  <th style="width:30%">Date</th>
                  <th>Answers</th>
                </tr>
              </thead>
              <tbody>
                @foreach($info_Question->Answer()->Get() as $Answer)
                <tr>
                  <td>{{ $Answer->created_at }}</td>
                  <td>{{ $Answer->answer }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
         </div>
      </div>
   </div>
</div>
@endsection
