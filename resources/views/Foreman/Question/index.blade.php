@extends('layouts.app')


@section('content')
<div class="page-title">
  <div class="title_left">
    <h3>Question <small></small></h3>
  </div>

  <div class="title_right">
    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
      <div class="input-group pull-right">
        <a href="/{{ Auth::User()->getRoleNames()[0] }}"><i class="fa fa-home"></i> Dashboard</a>
      </div>
    </div>
  </div>
</div>

<div class="clearfix"></div>
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="x_panel">
  <div class="x_title">
    <h2>Question Detail</small></h2>
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
    <table id="viewForm" class="table table-striped table-bordered bulk_action">
      <thead>
        <tr>
          <th>ID</th>
          <th>Report</th>
          <th>Question</th>
          <th>Answer</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
      <tfoot>
      	 <th colspan="3"><a class="btn btn-info" style="margin-right:2px;" id="btnUpdate" name="btnUpdate" href="javascript:void(0)" title="Update Data">Update</a>&nbsp;&nbsp;<span class="alertDone" style="color:#1ABB9C"></span></th></th>
      </tfoot>
    </table>
  </div>
</div>
</div>

@endsection

@push('scripts')

<script type="text/javascript">
    $('#viewForm').DataTable({
        "processing": true,
        "serverSide": true,
		"ajax": "{{url('/Foreman/Question/grid')}}",
        "columns": [
			{ data: 'id', name: 'id' },
			{ data: 'report_id', name: 'report_id' },
			{ data: 'question', name: 'question' },
			{ data: 'answer', name: 'answer' },
		],
		order: [ [0, 'desc'] ],
    });
	
	$('#btnUpdate').on('click', function (e) { 
		$( ".answerArea" ).each(function( index ) {
			$('.alertDone').html("");
			var question_id = $(this).data("question_id");
			var answer_id = "";
			var user_id = $(this).data("user_id");
			var answer = $(this).val();  
			
			e.preventDefault();		 
			var url = "{{ url('/Foreman/Question/UpdateAnswer/') }}";
			// confirm then
			$.ajax({
				url: url,
				type: 'POST',
				dataType: 'text',
				data: {method: '_POST', "question_id": question_id, "answer_id":answer_id, "user_id":user_id, "answer":answer,   "_token": "{{ csrf_token() }}" , submit: true},
				error: function (result, status, err) {
					//alert(result.responseText);
				},
			}).always(function (data) {
				$('.alertDone').html("Updated");
				$('#viewForm').DataTable().draw(false);
			});	
		});
		return false;
	});
	
	/*$('#viewForm').on('click', '#btnUpdate[data-question_id]', function (e) { 
		var question_id = $(this).data("question_id");
		var answer_id = $(this).data("answer_id");
		var user_id = $(this).data("user_id");
		var answer = $("#answer"+question_id).val();
		//alert(answer);
	
		e.preventDefault();		 
		var url = "{{ url('/Foreman/Question/UpdateAnswer/') }}";
		// confirm then
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'json',
			data: {method: '_POST', "question_id": question_id, "answer_id":answer_id, "user_id":user_id, "answer":answer,   "_token": "{{ csrf_token() }}" , submit: true},
			error: function (result, status, err) {
				//alert(result.responseText);
			},
		}).always(function (data) {
			$('#viewForm').DataTable().draw(false);
		});
		return false;
	});
*/
</script>

@endpush