@extends('layouts.app')
@section('content')
<div class="page-title">
  <div class="title_left">
    <h3>Foreman Report <small></small></h3>
  </div>

  <div class="title_right">
    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
      <div class="input-group pull-right">
        <a href="/{{ Auth::User()->getRoleNames()[0] }}"><i class="fa fa-home"></i> Dashboard</a> > <a href="/{{ Auth::User()->getRoleNames()[0] }}/Report"><i class="fa fa-users"></i> Report</a>
      </div>
    </div>
  </div>
</div>
<div class="row">
   <div class="col-md-8 col-xs-12">
      <div class="x_panel">
         <div class="x_title">
            <h2>View Report</h2>
            <div class="clearfix"></div>
         </div>
         <div class="x_content">
            <br>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                  <label>Name: </label>
			  	  {{ $info_Report->Foreman()->First()->name }}-{{ $info_Report->id }}
               </div>
               <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                  <label>Date: </label>
			  	  {{ $info_Report->created_at }}
               </div>
           </div>
           <hr />
           <table id="viewForm" class="table table-striped table-bordered bulk_action">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Hours</th>
                </tr>
              </thead>
              <tbody>
                @foreach($info_Report->Crew_report_detail()->Get() as $ReportDetail)
                <tr>
                  <td>
                    CREW-{{ $ReportDetail->crew_id }}
                  </td>
                  <td>{{ $ReportDetail->Crew()->First()->name }}</td>
                  <td>{{ $ReportDetail->hours }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
					@if($info_Report->comments!="")
                    	<label>Comments:</label>
                    	<p class="form-control">{{ $info_Report->comments }}</p>
                    @endif
               </div>
           </div>
         </div>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-md-8 col-xs-12">
      <div class="x_panel">
         <div class="x_title">
            <h2>View Questions ({{ $info_Report->Foreman()->First()->name }})</h2>
            <div class="clearfix"></div>
         </div>
         <div class="x_content">
            <br>
            @foreach($info_Question as $Question)
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
			      <label>QUESTION-{{ $Question->id }}: </label>
			  	  {{ $Question->question }} 
                  <a href="javascript:void(0)" data-question_id="{{ $Question->id }}" id="toggleAnswer" class="pull-right"><i class="fa fa-plus"></i> Detail</a>
               </div>
           </div>
           <div id="{{ $Question->id }}" style="display:none" >
               <table class="table table-striped table-bordered bulk_action">
                  <thead>
                    <tr>
                      <th>Date</th>
                      <th>Answers</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($Question->Answer()->Orderby('created_at','desc')->Get() as $Answer)
                    <tr>
                      <td>{{ $Answer->created_at }}</td>
                      <td>
                            <textarea id="answer{{ $Answer->id }}" class="form-control" cols="40" rows="2">{{ $Answer->answer }}</textarea>
                       </td>
                       <td style="width:20%; vertical-align:bottom">
                            <a class="btn btn-info" style="margin-right:2px;" id="btnUpdate" name="btnUpdate" data-question_id="{{ $Question->id }}" data-answer_id="{{ $Answer->id }}" data-user_id="{{ $Question->user_id }}" data-question_id="" href="javascript:void(0)" title="Update Data">Update</a>
                       </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
            	<hr />
            </div>
            <hr />
            @endforeach
         </div>
      </div>
   </div>
</div>
@endsection

@push('scripts')

<script type="text/javascript">
	
	$(document).on('click', '#btnUpdate[data-question_id]', function (e) { 
		var question_id = $(this).data("question_id");
		var answer_id = $(this).data("answer_id");
		var user_id = $(this).data("user_id");
		var answer = $("#answer"+answer_id).val();
		//alert(answer +"- "+ question_id +"- "+ answer_id +"- "+ user_id);
	
		e.preventDefault();		 
		var url = "{{ url('/Foreman/Question/UpdateAnswer/') }}";
		// confirm then
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'json',
			data: {method: '_POST', "question_id": question_id, "answer_id":answer_id, "user_id":user_id, "answer":answer,   "_token": "{{ csrf_token() }}" , submit: true},
			error: function (result, status, err) {
				//alert(result.responseText);
			}
		})
		return false;
	});
	
	$(document).on('click', '#toggleAnswer[data-question_id]', function (e) { 
		var question_id = $(this).data("question_id");
		var e = document.getElementById(question_id);
		if(e.style.display == 'block')
		{
			$(this).html('<i class="fa fa-plus"></i> Detail');
			e.style.display = 'none';
		}
		else
		{
			$(this).html('<i class="fa fa-minus"></i> Detail');
			e.style.display = 'block';
		}
		return false;
	});


</script>

@endpush
