@component('mail::message')

A report has been added

<?php
$url = 'http://192.168.198.153:8000/Superintendent/Report/rpt'.base64_encode($crew_report->id*25).'/edit';
?>
@component('mail::button', ['url' => $url])
Verify
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
