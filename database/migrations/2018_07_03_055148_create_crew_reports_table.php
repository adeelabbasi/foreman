<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrewReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('crew_reports', function (Blueprint $table) {
			$table->increments('id');
            $table->integer('foreman_id')->unsigned();
			$table->integer('superintendent_id')->unsigned()->nullable();
			$table->char('status',1);
			$table->string('comments')->nullable();
            $table->timestamps();
        });
		
		Schema::table('crew_reports', function(Blueprint $table) {
			$table->foreign('foreman_id')->references('id')->on('users')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
		
		Schema::table('crew_reports', function(Blueprint $table) {
			$table->foreign('superintendent_id')->references('id')->on('users')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crew_reports');
    }
}
