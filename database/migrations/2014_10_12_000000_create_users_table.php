<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
			$table->string('username')->unique();
			$table->integer('job_id')->unsigned()->nullable();
			$table->integer('superintendent_id')->unsigned()->nullable();
            $table->string('email')->unique();
            $table->string('password');
			$table->string('avatar');
			$table->char('type',1);
            $table->rememberToken();
            $table->timestamps();
        });
		
		Schema::table('users', function(Blueprint $table) {
			$table->foreign('job_id')->references('id')->on('jobs')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
		
		Schema::table('users', function(Blueprint $table) {
			$table->foreign('superintendent_id')->references('id')->on('users')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
