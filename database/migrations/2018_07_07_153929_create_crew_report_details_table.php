<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrewReportDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crew_report_details', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('crew_id')->unsigned()->nullable();
			$table->integer('report_id')->unsigned();
			$table->integer('hours');
			$table->timestamps();
        });
		
		Schema::table('crew_report_details', function(Blueprint $table) {
			$table->foreign('crew_id')->references('id')->on('crews')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
		
		Schema::table('crew_report_details', function(Blueprint $table) {
			$table->foreign('report_id')->references('id')->on('crew_reports')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crew_report_details');
    }
}
